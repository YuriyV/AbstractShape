﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    class Triangle : AbstractShape , ICarried
    {
        
        private double a;
        private double b;
        private double c;
        private static double calculation(double a, double b, double c)
        {
            double p = (a + b + c) / 2;
            double rrr = p * (p - a) * (p - b) * (p - c);
            return rrr;
        }

        static public Triangle create(double a, double b, double c)
        {
            double rr = calculation(a,b,c);
            
            if (rr <= 0)
            {
                return null;
            }
            else
            {
                return new Triangle(a, b, c);
            }
        }
        private Triangle(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public override double getArea()
        {  
              return Math.Sqrt(calculation(a, b, c));
        }

        public override string Display()
        {
            string ret = "Треугольник. Площадь равна: " + Math.Round(getArea(), 4);
            return ret;
        }
        public bool canCarry()
        {
            return true;
        }

    }
}
