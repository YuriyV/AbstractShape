﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    class Square : AbstractRectangleShape
    {
        public Square(double length) : base(length, length)
        {
        }

        public override string Display ()
        {
            return "Квадрат. Площадь равна: " + Math.Round(getArea(), 4);
        }
    }
}
