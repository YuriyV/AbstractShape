﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    abstract class AbstractRoundShape: AbstractShape , IRollable
    {
        protected const double Pi = 3.14;
        protected double radius;

        protected AbstractRoundShape(double radius)
        {
            this.radius = radius;
            
        }
        public abstract bool canRoll();
    }
}
