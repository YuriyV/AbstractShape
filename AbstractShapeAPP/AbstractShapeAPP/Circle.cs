﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    class Circle : AbstractRoundShape
    {
        public Circle (double radius) : base(radius)
        {
        }
        public override double getArea()
        {
            return radius * radius * Pi;
        }

        public override string Display()
        {
            return "Круг. Площадь равна: " + Math.Round(getArea(), 4);
        }

        public override bool canRoll()
        {
            return true;
        }
    }
}
