﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    class Rectangle : AbstractRectangleShape
    {
        public Rectangle(double length, double width) : base(length, width)
        {
        }

        public override string Display ()
        {
            return "Прямоугольник. Площадь равна: " + Math.Round(getArea(), 4);
        }
    }
}
