﻿using System.Collections.Generic;
using System.Windows.Forms;
using System;

namespace AbstractShapeAPP
{
    public partial class Form1 : Form
    {
        List<AbstractShape> shapes = new List<AbstractShape>();

        public Form1()
        {
            InitializeComponent();
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            AbstractShape shape = null;
            Random rnd = new Random();
            int _rnd = rnd.Next(1,6);
            switch (_rnd)
            {
                case 1:
                    shape = new Square(rnd.NextDouble() * 10);
                    break;
                case 2:
                    shape = new Ellipse(rnd.NextDouble() * 10, rnd.NextDouble() * 10);
                    break;
                case 3:
                    bool created = false;
                    int i = 0;
                    while (!created && i < 10)
                    {
                        shape = Triangle.create(rnd.NextDouble() * 10, rnd.NextDouble() * 10, rnd.NextDouble() * 10);
                        if (shape != null)
                            created = true;
                        i++;
                    }
                        //new Triangle(1.0, 2.0, 3.0);
                    break;
                case 4:
                    shape = new Rectangle(rnd.NextDouble() * 10, rnd.NextDouble() * 10);
                    break;
                case 5:
                    shape = new Circle(rnd.NextDouble() * 10);
                    break;
            }
           shapes.Add(shape);


        }

        private void printButton_Click(object sender, EventArgs e)
        {
            if (shapes.Count == 0)
            {
                MessageBox.Show("You must generate the shape before printing!");
            }
            else
            {
                for (int i = 0; i < shapes.Count; i++)
                { 
                    if(shapes[i] is IRollable)
                    {
                        if(((AbstractRoundShape)shapes[i]).canRoll())
                        {
                            textBox1.Text = textBox1.Text + shapes[i].Display() + Environment.NewLine;
                        }                       
                    }
                }
                for (int i = 0; i < shapes.Count; i++)
                {
                    if (shapes[i] is ICarried)
                    {
                        //довести проверку до ума
                        if (shapes[i] is AbstractRectangleShape)
                        {
                            if (((AbstractRectangleShape)shapes[i]).canCarry())
                            {
                                textBox1.Text = textBox1.Text + shapes[i].Display() + Environment.NewLine;
                            }
                            else
                            {

                            }
                        }
                        else if (shapes[i] is Triangle)
                        {

                        }
                    }
                }

                
            }
            shapes.Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        

    }
}
