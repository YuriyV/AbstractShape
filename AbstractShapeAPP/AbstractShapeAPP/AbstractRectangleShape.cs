﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    abstract class AbstractRectangleShape: AbstractShape , ICarried
    {
        protected double length;
        protected double width;

        protected AbstractRectangleShape(double length, double width)
        {
            this.length = length;
            this.width = width;
        }


        public override double getArea()
        {  
            return length * width;
        }

        public bool canCarry()
    {
        return true;
    }
    }
}
