﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    abstract class AbstractShape
    {
        public abstract double getArea();
        public abstract string Display();
    }
}
