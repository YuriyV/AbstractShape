﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractShapeAPP
{
    class Ellipse : AbstractRoundShape
    {
        double radius2;
        public Ellipse (double radius, double radius2) : base(radius)
        {
            this.radius2 = radius2;
        }
        
        public override double getArea()
        {
            return radius * radius2 * Pi;
        }

        public override string Display()
        {
            return "Овал. Площадь равна: " + Math.Round(getArea(), 4);
        }

        public override bool canRoll()
        {
            return false;
        }
    }
}
